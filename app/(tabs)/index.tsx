import React, { useEffect, useState } from "react";
import { SafeAreaView, ScrollView, View, Text, StyleSheet } from "react-native";
import { RadioButton, Button, PaperProvider, Portal } from "react-native-paper";
import ModelComponent from "../components/modal";

const questionData = [
    {
        question: "What is the capital of France?",
        answers: ["Paris", "London", "Berlin", "Madrid"],
        correctAnswer: "Paris",
    },
    {
        question: "What is 2 + 2?",
        answers: ["3", "4", "5", "6"],
        correctAnswer: "4",
    },
    {
        question: "Which planet is known as the Red Planet?",
        answers: ["Mars", "Venus", "Jupiter", "Saturn"],
        correctAnswer: "Mars",
    },
    {
        question: "What is the largest ocean on Earth?",
        answers: ["Pacific Ocean", "Atlantic Ocean", "Indian Ocean", "Arctic Ocean"],
        correctAnswer: "Pacific Ocean",
    },
    {
        question: "What is the smallest prime number?",
        answers: ["2", "1", "3", "5"],
        correctAnswer: "2",
    },
    {
        question: "Who wrote 'Romeo and Juliet'?",
        answers: ["William Shakespeare", "Charles Dickens", "Mark Twain", "Jane Austen"],
        correctAnswer: "William Shakespeare",
    },
    {
        question: "What is the capital of Japan?",
        answers: ["Tokyo", "Kyoto", "Osaka", "Nagoya"],
        correctAnswer: "Tokyo",
    },
    {
        question: "What is the square root of 64?",
        answers: ["8", "6", "7", "9"],
        correctAnswer: "8",
    },
    {
        question: "Who painted the Mona Lisa?",
        answers: ["Leonardo da Vinci", "Vincent van Gogh", "Pablo Picasso", "Claude Monet"],
        correctAnswer: "Leonardo da Vinci",
    },
    {
        question: "Which gas do plants absorb from the atmosphere?",
        answers: ["Carbon dioxide", "Oxygen", "Nitrogen", "Hydrogen"],
        correctAnswer: "Carbon dioxide",
    },
    {
        question: "What is the largest mammal?",
        answers: ["Blue whale", "Elephant", "Giraffe", "Hippopotamus"],
        correctAnswer: "Blue whale",
    },
    {
        question: "Who is known as the Father of Computers?",
        answers: ["Charles Babbage", "Alan Turing", "Bill Gates", "Steve Jobs"],
        correctAnswer: "Charles Babbage",
    },
    {
        question: "What is the chemical symbol for water?",
        answers: ["H2O", "O2", "CO2", "NaCl"],
        correctAnswer: "H2O",
    },
    {
        question: "What is the hardest natural substance on Earth?",
        answers: ["Diamond", "Gold", "Iron", "Platinum"],
        correctAnswer: "Diamond",
    },
    {
        question: "Which planet is closest to the sun?",
        answers: ["Mercury", "Venus", "Earth", "Mars"],
        correctAnswer: "Mercury",
    },
    {
        question: "What is the main ingredient in sushi?",
        answers: ["Rice", "Fish", "Seaweed", "Soy sauce"],
        correctAnswer: "Rice",
    },
    {
        question: "Which element has the atomic number 1?",
        answers: ["Hydrogen", "Helium", "Oxygen", "Carbon"],
        correctAnswer: "Hydrogen",
    },
    {
        question: "Who invented the telephone?",
        answers: ["Alexander Graham Bell", "Thomas Edison", "Nikola Tesla", "Guglielmo Marconi"],
        correctAnswer: "Alexander Graham Bell",
    },
    {
        question: "Which country is known as the Land of the Rising Sun?",
        answers: ["Japan", "China", "South Korea", "Thailand"],
        correctAnswer: "Japan",
    },
    {
        question: "What is the tallest mountain in the world?",
        answers: ["Mount Everest", "K2", "Kangchenjunga", "Lhotse"],
        correctAnswer: "Mount Everest",
    },
];

type QuestionProps = {
    num: number;
    question: string;
    answers: string[];
    selectedAnswer: string | null;
    onSelect: (answer: string) => void;
};

const Question: React.FC<QuestionProps> = ({ num, question, answers, selectedAnswer, onSelect }) => {
    return (
        <View style={styles.questionContainer}>
            <Text style={styles.questionText}>{`${num + 1}. ${question}`}</Text>
            <RadioButton.Group onValueChange={onSelect} value={selectedAnswer}>
                {answers.map((answer, index) => (
                    <RadioButton.Item key={index} label={answer} value={answer} />
                ))}
            </RadioButton.Group>
        </View>
    );
};

const App: React.FC = () => {
    const [questions, setQuestions] = useState(questionData.map((q) => ({ ...q, selectedAnswer: null as string | null })));
    const [showResults, setShowResults] = useState(false);

    useEffect(() => {
        const shuffledQuestions = questionData
            .sort(() => 0.5 - Math.random())
            .slice(0, 20)
            .map((q) => ({
                ...q,
                answers: q.answers.sort(() => 0.5 - Math.random()),
                selectedAnswer: null,
            }));
        setQuestions(shuffledQuestions);
    }, []);

    const handleSelectAnswer = (index: number, answer: string) => {
        const newQuestions = [...questions];
        newQuestions[index].selectedAnswer = answer;
        setQuestions(newQuestions);
    };

    const handleSubmitAnswers = () => {
        // Check if all questions are answered
        const allQuestionsAnswered = questions.every((q) => q.selectedAnswer !== null);
        if (allQuestionsAnswered) {
            setShowResults(true);
        } else {
            // Show an alert or message to indicate that all questions must be answered
            alert("Please answer all questions before submitting.");
        }
    };

    const calculateScore = () => {
        let score = 0;
        questions.forEach((q, index) => {
            if (q.selectedAnswer === q.correctAnswer) {
                score++;
            }
        });
        return score;
    };

    return (
        <PaperProvider>
            <SafeAreaView style={styles.container}>
                <ScrollView contentContainerStyle={styles.scrollViewContent}>
                    {questions.map((q, index) => (
                        <Question
                            key={index}
                            num={index}
                            question={q.question}
                            answers={q.answers}
                            selectedAnswer={q.selectedAnswer}
                            onSelect={(answer: string) => handleSelectAnswer(index, answer)}
                        />
                    ))}
                    {/* <ModelComponent handleSubmitAnswers={handleSubmitAnswers} /> */}

                    <Button mode="contained" onPress={handleSubmitAnswers}>
                        Submit Answers
                    </Button>
                    {showResults && (
                        <View style={styles.resultsContainer}>
                            <Text style={styles.resultsText}>
                                Your score: {calculateScore()} / {questions.length}
                            </Text>
                            {/* <Text style={styles.resultsText}>Correct answers:</Text>
                            {questions.map((q, index) => (
                                <Text key={index} style={styles.resultsText}>
                                    {q.question}: {q.correctAnswer}
                                </Text>
                            ))} */}
                        </View>
                    )}
                </ScrollView>
            </SafeAreaView>
        </PaperProvider>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#f5f5f5",
    },
    scrollViewContent: {
        padding: 10,
    },
    questionContainer: {
        marginBottom: 20,
        padding: 15,
        backgroundColor: "#fff",
        borderRadius: 10,
        shadowColor: "#000",
        shadowOpacity: 0.1,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 8,
        elevation: 5,
    },
    questionText: {
        fontSize: 18,
        marginBottom: 10,
    },
    answerText: {
        color: "#000000 !important",
    },
    submitButton: {
        marginTop: 20,
        alignSelf: "center",
    },
    resultsContainer: {
        marginTop: 20,
        padding: 15,
        backgroundColor: "#fff",
        borderRadius: 10,
        shadowColor: "#000",
        shadowOpacity: 0.1,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 8,
        elevation: 5,
    },
    resultsText: {
        fontSize: 16,
        marginBottom: 10,
    },
});

export default App;
