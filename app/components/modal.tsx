import * as React from "react";
import { Modal, Portal, Text, Button, PaperProvider, TextInput } from "react-native-paper";

interface ChildProps {
    handleSubmitAnswers: () => void;
}

const ModelComponent: React.FC<ChildProps> = ({ handleSubmitAnswers }) => {
    const [visible, setVisible] = React.useState<boolean>(false);
    const [text, setText] = React.useState("");

    const showModal = () => setVisible(true);
    const hideModal = () => setVisible(false);
    const containerStyle = { backgroundColor: "white", padding: 20 };
    const saveModal = () => {
        console.log(text);
        setVisible(false);
    };
    const submitModal = () => {
        handleSubmitAnswers();
        showModal();
    };

    return (
        <>
            <Portal>
                <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
                    <Text variant="labelMedium">Please enter your name</Text>
                    <TextInput label="Name" value={text} onChangeText={(text) => setText(text)} />

                    <Button mode="contained" style={{ marginTop: 30 }} onPress={saveModal}>
                        save
                    </Button>
                </Modal>
            </Portal>
            <Button mode="contained" style={{ marginTop: 30 }} onPress={submitModal}>
                Submit Answers
            </Button>
        </>
    );
};

export default ModelComponent;
